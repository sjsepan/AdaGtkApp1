with Gtk.Main;
with Gtk.Window;      use Gtk.Window;
with Gtk.Grid;        use Gtk.Grid;
with Gtk.Widget;     use Gtk.Widget;
with Gtk.Button;      use Gtk.Button;
with Gtkada.Handlers; use Gtkada.Handlers;
with Glib.Object;

--with Main_quit;
with adagtkapp1_callbacks; use adagtkapp1_callbacks;

procedure adagtkapp1 is
   FrmMain : Gtk_Window;
   Grid   : Gtk_Grid;
   ButtonOneOrTwo : Gtk_Button;
   BtnQuit : Gtk_Button;

begin
   --  Initialize GtkAda.
   Gtk.Main.Init;

   -- create a top level window
   Gtk_New (FrmMain);
   FrmMain.Set_Title ("Ada Gtk App 1");
   -- set the (interior) border width (margins) of the window (client area)
   FrmMain.Set_Border_Width (10);

   -- When the window emits the "delete-event" signal (which is emitted
   --  by GTK+ in response to an event coming from the window manager,
   --  usually as a result of clicking the "close" window control), we
   --  ask it to call the on_delete_event() function as defined above.
   -- This can also be triggered by a button (in this case) or menu click.
   FrmMain.On_Delete_Event (FrmMain_OnDeleteEvent'Access);

   -- connect the "destroy" signal
   -- Note: does not call Gtk.Main.Main_Quit directly
   FrmMain.On_Destroy (FrmMain_OnDestroy'Access);

   -- Here we construct the container that is going pack our buttons
   Gtk_New (Grid);
   -- Packed the container in the Window
   FrmMain.Add (Grid);

   -- create a button with label
   Gtk_New (ButtonOneOrTwo, "Button 1");
   -- connect the click signal
   ButtonOneOrTwo.On_Clicked (BtnOneOrTwo_OnClicked'Access);
   -- Place the first button in the grid cell (0, 0), and make it fill
   -- just 1 cell horizontally and vertically (ie no spanning)
   Grid.Attach (ButtonOneOrTwo, 0, 0, 1, 1);

   -- create another button with label
   Gtk_New (ButtonOneOrTwo, "Button 2");
   ButtonOneOrTwo.On_Clicked (BtnOneOrTwo_OnClicked'Access);
   -- Place the second button in the grid cell (1, 0), and make it fill
   -- just 1 cell horizontally and vertically (ie no spanning)
   Grid.Attach (ButtonOneOrTwo, 1, 0, 1, 1);

   -- create a button with label
   Gtk_New (BtnQuit, "Quit");
   -- Connect the click signal
   -- Note:this does not do nothing, while the Widget_Callback.Object_Connect call below
   --  connects an alternate click event receiving the main form object
   BtnQuit.On_Clicked (BtnQuit_OnClicked'Access);
   -- Connect the "clicked" signal of the button to a callback function,
   --  that will close the Win when button is clicked.
   -- This is the alternate version of the callback, 
   --  that passes the main form object instead of the button object.
   Widget_Callback.Object_Connect (
      BtnQuit,
      "clicked",
      Widget_Callback.To_Marshaller (BtnQuit_OnClicked2'Access),
      FrmMain
   );
   -- Place the Quit button in the grid cell (0, 1), and make it
   -- span 2 columns.
   Grid.Attach (BtnQuit, 0, 1, 2, 1);
   
   -- Now that we are done packing our widgets, we show them all
   -- in one go, by calling FrmMain.Show_All.
   -- This call recursively calls Show on all widgets
   -- that are contained in the window, directly or indirectly.
   FrmMain.Show_All;

   -- All GTK applications must have a Gtk.Main.Main. This is the main message loop.
   -- Control ends here and waits for an event to occur 
   -- (like a key press or a mouse event),  until Gtk.Main.Main_Quit is called.
   Gtk.Main.Main;
end adagtkapp1;
