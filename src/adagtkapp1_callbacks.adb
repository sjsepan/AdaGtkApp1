with Gtk.Window;       use Gtk.Window;
with Gtk.Widget;       use Gtk.Widget;
with Ada.Text_IO; use Ada.Text_IO;
with Gtk.Main;
with Glib.Object;

package body adagtkapp1_callbacks is
   -- If you return false in the "delete_event" signal handler,
   -- GTK will emit the "destroy" signal. Returning true means
   -- you don't want the window to be destroyed.
   --
   -- This is useful for popping up 'are you sure you want to quit?'
   -- type dialogs.
   function FrmMain_OnDeleteEvent (Self: access Gtk_Widget_Record'Class; Event: Gdk.Event.Gdk_Event) return  Boolean is
      isCancel : Boolean := False;
   begin
      Put_Line ("FrmMain_OnDeleteEvent; return False will allow Destroy");

      --This is the handler where the choice to Quit confirmation is made, 
      -- so this is where user prompt should happen, 
      -- and all handlers for quitting/closing should redirect here, 
      -- possibly by calling the form Delete to trigger this.
      isCancel := False;

      return isCancel;
   end FrmMain_OnDeleteEvent;

   procedure FrmMain_OnDestroy (Self : access Gtk_Widget_Record'Class) is
   begin
      -- Gtk.Main.Main is waiting for an event to occur (like a key press or a mouse event),
      -- until Gtk.Main.Main_Quit is called.
      Put_Line ("FrmMain_OnDestroy; calls Main_Quit");
      Gtk.Main.Main_Quit;
   end FrmMain_OnDestroy;

   procedure BtnOneOrTwo_OnClicked (Self : access Gtk_Button_Record'Class) is
   begin
      Put_Line ("BtnOneOrTwo_OnClicked");
   end BtnOneOrTwo_OnClicked;

   procedure BtnQuit_OnClicked (Self : access Gtk_Button_Record'Class) is
--  procedure BtnQuit_OnClicked (Win : access Gtk_Widget_Record'Class) is
    begin
      Put_Line ("BtnQuit_OnClicked; no action");
      --  Gtk.Window.Close (Self.Get_Parent);
   end BtnQuit_OnClicked;

   procedure BtnQuit_OnClicked2 (Self : access Gtk_Widget_Record'Class) is
   begin
      Put_Line ("BtnQuit_OnClicked2; calls Gtk.Window.Close on Self");
      --TODO:this should route to FrmMain_OnDeleteEvent instead, and let that prompt user.
      --Note:if so, then it is not necessary to use this form of handler, as FrmMain_OnDeleteEvent
      -- will have access to main form object
      --  Destroy (Self); --Self is main form object, not button
      Gtk.Window.Close (Gtk_Window(Self));
   end BtnQuit_OnClicked2;

end adagtkapp1_callbacks;